/*
 * TextureHolder.cpp
 *
 *  Created on: 3 ene. 2019
 *      Author: joe
 */
#include "TextureHolder.h"
// Include the "assert feature"
#include <assert.h>

using namespace sf;
using namespace std;

TextureHolder* TextureHolder::m_s_Instance=nullptr;

TextureHolder::TextureHolder() {
	assert(m_s_Instance==nullptr);
	m_s_Instance = this;
}

Texture& TextureHolder::GetTexture(string const& filename)
{
	// Get a reference to m_Textures using m_s_Instance
	auto& m = m_s_Instance->m_Textures;
	// auto is the equivalent of map<string, Texture>

	// Create an iterador to hold a key-value-pair (kvp)
	// and search for the required kvp
	// using the passed in filename
	auto keyValuePair = m.find(filename);
	// auto is equivelant of map<string, Texture>::iterator
	// Did we find a match?
	if (keyValuePair != m.end()) {
		// Yes
		// Return the texture
		// the second part of the kvp, the texture
		return keyValuePair->second;
	}
	else {
		// FIlename not found
		// Create a new key value pair using filename

		auto& texture=m[filename];
		// Load the texture form file in the usual way
		texture.loadFromFile(filename);

		// Return the texture to the calling code
		return texture;
	}
}
