/*
 * ZombieArena.cpp
 *
 *  Created on: 23 dic. 2018
 *      Author: joe
 */
#include <SFML/Graphics.hpp>
#include "Player.h"
#include "ZombieArena.h"
#include <iostream>
#include <assert.h>
#include "TextureHolder.h"
#include "Bullet.h"
#include "Pickup.h"

using namespace sf;

int main()
{
	// Here is teh instance of TextureHolder
	TextureHolder holder;

	// The game will always be in one of four states
	enum class State {
		PAUSED, LEVELING_UP, GAME_OVER, PLAYING
	};

	// Start with GAME OVER state
	State state = State::GAME_OVER;

	// Get the Screen resolution and create a SFML Window
	Vector2f resolution;
	resolution.x=VideoMode::getDesktopMode().width; // es una función estática
	resolution.y=VideoMode::getDesktopMode().height;
	// resolution.x=1920;
	// resolution.y=1080;
	std::cout << "Resolución (x,y)=(" << resolution.x << "," << resolution.y << ")" << std::endl;

	RenderWindow window(VideoMode(resolution.x, resolution.y), "Zombie Arena", Style::Fullscreen);

	// Create a an SFML View for the main action
	View mainView(FloatRect(0.0f, 0.0f, resolution.x, resolution.y));

	// Here is our clock for timing everything
	Clock clock;

	// How long has the PLAYING state been active
	Time gameTimeTotal;

	// Where is the mouse in relation to world coordinates
	Vector2f mouseWorldPosition; // crosshair

	// Where is the mouse in relation to screen coordinates
	Vector2i mouseScreenPosition;

	// Create an instance of the Player class
	Player player;

	// The boundaries of the arena
	IntRect arena;

	// Create the background VertexArray background;
	VertexArray background;
	// Load the texture for our background vertex array
	Texture textureBackground = TextureHolder::GetTexture("graphics/background_sheet.png");
	//assert(static_cast <int>(textureBackground.loadFromFile("graphics/background_sheet.png")));

	// Pepare for a horde of zombies
	int numZombies;
	int numZombiesAlive;
	Zombie* zombies =nullptr;

	// 100 bullets should do
	Bullet bullets[100];
	int currentBullet = 0U;
	int bulletsSpare = 24U;
	int bulletsInClip = 6U;
	int clipSize = 6U;
	float fireRate = 1.0F;
	// When was the fire button last pressed?
	Time lastPressed;

	// Hide the mouse pointer and replace it with crosshair
	window.setMouseCursorVisible(false);
	Sprite spriteCrosshair;
	Texture textureCrosshair = TextureHolder::GetTexture("graphics/crosshair.png");

	spriteCrosshair.setTexture(textureCrosshair);
	spriteCrosshair.setOrigin(25.0F, 25.0F);

	// Create a couple of pickups
	Pickup healthPickup(1);
	Pickup ammoPickup(2);

	// The main game loop
	while (window.isOpen())
	{
		/*
		 ******************
		 Handle input
		 ******************
		 */

		// Handle evets by polling
		Event event;
		while (window.pollEvent(event)) {
			if (event.type == Event::KeyPressed) {
				// Pause a game while playing
				if (event.key.code == Keyboard::Return && state == State::PLAYING) 	{
					state=State::PAUSED;
				}
				// Restart while paused
				else if (event.key.code == Keyboard::Return && state == State::PAUSED) {
					state=State::PLAYING;
					// Reset the clock so ther isn't a frame jump
					clock.restart();
				}
				// Start a new game while in GAME_OVER state
				else if (event.key.code == Keyboard::Return && state == State::GAME_OVER) {
					state = State::LEVELING_UP;
				}
				if (state == State::PLAYING) {
					// Reloading
					if (event.key.code == Keyboard::R) {
						if (bulletsSpare >= clipSize) {
							// Plenty of bullets. Reload
							bulletsInClip = clipSize;
							bulletsSpare -= clipSize;
						}
						else if (bulletsSpare > 0) {
							// ONly few bullets left
							bulletsInClip = bulletsSpare;
							bulletsSpare = 0;
						}
						else {
							// MOre here soon?!
						}
					}
				}
			}
		} // End event polling

		// Handle the player quitting
		if (Keyboard::isKeyPressed(Keyboard::Escape)) {
			window.close();
		}

		// Handle WASD while playing
		if (state == State::PLAYING) {
			// Handle the pressing and releasing fo the WASD keys
			if (Keyboard::isKeyPressed(Keyboard::W)) {
				player.moveUp();
			} else {
				player.stopUp();
			}
			if (Keyboard::isKeyPressed(Keyboard::S)) {
				player.moveDown();
			} else {
				player.stopDown();
			}
			if (Keyboard::isKeyPressed(Keyboard::A)) {
				player.moveLeft();
			} else {
				player.stopLeft();
			}
			if (Keyboard::isKeyPressed(Keyboard::D)) {
				player.moveRight();
			} else {
				player.stopRight();
			}

			// Fire a bullet
			if (Mouse::isButtonPressed(sf::Mouse::Left)) {
				if (gameTimeTotal.asMilliseconds() - lastPressed.asMilliseconds() \
					> 1000 / fireRate && bulletsInClip > 0) {
					// Pass the center of the player
					// and the center of the crosshair
					// to the shoot function
					bullets[currentBullet].shoot(player.getCenter().x, player.getCenter().y, \
												 mouseWorldPosition.x, mouseWorldPosition.y);
					currentBullet++;
					if (currentBullet > 99) {
						currentBullet = 0;
					}
					lastPressed = gameTimeTotal;
					bulletsInClip--;
				}
			} // End fire a bullet
		} // End WASD R while playing

		// Handle the LEVELING up state
		if (state == State::LEVELING_UP) {
			// Handle the player LEVELING up
			if (event.key.code == Keyboard::Num1) {
				state=State::PLAYING;
			}
			if (event.key.code == Keyboard::Num2) {
				state=State::PLAYING;
			}
			if (event.key.code == Keyboard::Num3) {
				state=State::PLAYING;
			}
			if (event.key.code == Keyboard::Num4) {
				state = State::PLAYING;
			}
			if (event.key.code == Keyboard::Num5) {
				state = State::PLAYING;
			}
			if (event.key.code == Keyboard::Num6) {
				state = State::PLAYING;
			}
			if (state == State::PLAYING) {
				// Prepare the level
				// We will modify the next two lines later
				arena.width = 500;
				arena.height = 500;
				arena.left = 0;
				arena.top = 0;

				// Pass the vertex array by reference
				// to the createBackground function
				int tileSize = createBackground(background, arena);

				// We will modify this line of code later
				// int tileSize=50;

				// Spawn the player in the middle of the arena
				player.spawn(arena, resolution, tileSize);

				// COnfigure the pickups
				healthPickup.setArena(arena);
				ammoPickup.setArena(arena);

				// Create a horde of zombies
				numZombies = 10;

				// Delete the previous allocatd memory if exists
				if (zombies != nullptr)
					delete[] zombies;

				zombies = createHorde(numZombies, arena);
				numZombiesAlive = numZombies;

				// Reset the clock so there isn't a frame jump
				clock.restart();
			}
		}// End LEVELING up

		/*
		 ****************
		 UPDATE THE FRAME
		 ****************
		 */
		if (state == State::PLAYING) {
			// Update the delta time
			Time dt=clock.restart();

			// Update the total game time
			gameTimeTotal += dt;

			// Make a decimal fraction of 1 from the delta time
			float dtAsSeconds=dt.asSeconds();

			// Where is the mouse pointer
			mouseScreenPosition=Mouse::getPosition();

			// Convert mouse position to world coordinates of mainView
			mouseWorldPosition=window.mapPixelToCoords(Mouse::getPosition(), mainView);

			// Set the crosshair to the mouse world location
			spriteCrosshair.setPosition(mouseWorldPosition);

			// Update the player
			player.update(dtAsSeconds, Mouse::getPosition());

			// Make a note of the players new position
			Vector2f playerPosition(player.getCenter());

			// Make the view centre around the player
			mainView.setCenter(player.getCenter());

			// Loop through each Zombie and update them
			for (int i = 0; i < numZombies; i++) {
				if (zombies[i].isAlive()) {
					zombies[i].update(dt.asSeconds(), playerPosition);
				}
			}

			// Update any bullets that are in-flight
			for (int i = 0; i < 100; i++) {
				if (bullets[i].isInFlight()) {
					bullets[i].update(dtAsSeconds);
				}
			}

			// Update the pickups
			healthPickup.update(dtAsSeconds);
			ammoPickup.update(dtAsSeconds);


		} // End updating the scene
		/*
		 **************
		 Draw the scene
		 **************
		 */
		if (state == State::PLAYING) {
			window.clear();
			// step the mainView to be displayed in the window
			// And draw everything related to it
			window.setView(mainView);

			// Draw the background
			window.draw(background, &textureBackground);

			// Draw the zombies
			for (int i = 0; i < numZombies; i++)
				window.draw(zombies[i].getSprite());

			for (int i = 0; i < 100; i++) {
				if (bullets[i].isInFlight()) {
					window.draw(bullets[i].getShape());
				}
			}

			// Draw the player
			window.draw(player.getSprite());

			// Draw the pickups, if currently spawned
			if (ammoPickup.isSpawned()) {
				window.draw(ammoPickup.getSprite());
			}

			if (healthPickup.isSpawned()) {
				window.draw(healthPickup.getSprite());
			}

			// Draw the crosshair
			window.draw(spriteCrosshair);
		}
		if (state == State::LEVELING_UP) {
		}
		if (state == State::PAUSED) {
		}
		if (state == State::GAME_OVER) {
		}
		window.display();
	} // End game loop
	delete[] zombies;
	zombies=nullptr;

	return 0;
}
