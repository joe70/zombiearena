/*
 * ZombieArena.h
 *
 *  Created on: 27 dic. 2018
 *      Author: joe
 */

#ifndef INCLUDE_ZOMBIEARENA_H_
#define INCLUDE_ZOMBIEARENA_H_

#include <SFML/Graphics.hpp>
#include "Zombie.h"
//using namespace sf;

int createBackground(sf::VertexArray& rVA, sf::IntRect arena);
Zombie* createHorde(int numZombies, sf::IntRect arena);
#endif /* INCLUDE_ZOMBIEARENA_H_ */
