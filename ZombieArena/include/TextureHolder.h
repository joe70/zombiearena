/*
 * TextureHolder.h
 *
 *  Created on: 2 ene. 2019
 *      Author: joe
 */

#ifndef INCLUDE_TEXTUREHOLDER_H_
#define INCLUDE_TEXTUREHOLDER_H_

#include <SFML/Graphics.hpp>
#include <map>

class TextureHolder {
private:
	// A map container from the STL,
	// that holds related pairs of String and Texture
	std::map<std::string, sf::Texture> m_Textures;

	// A pointer of the same type as the class itself
	// the one and only instance
	static TextureHolder* m_s_Instance;
public:
	TextureHolder();
	static sf::Texture& GetTexture(std::string const& filename);
};
#endif /* INCLUDE_TEXTUREHOLDER_H_ */
