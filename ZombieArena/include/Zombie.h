/*
 * Zombie.h
 *
 *  Created on: 3 ene. 2019
 *      Author: joe
 */

#ifndef INCLUDE_ZOMBIE_H_
#define INCLUDE_ZOMBIE_H_

#include <SFML/Graphics.hpp>

class Zombie {
private:
	// How fast is each zombie type?
	const float BLOATER_SPEED = 40.0f; // un tanto veloces, hinchados
	const float CHASER_SPEED = 80.0f; 	// los más rápidos, perseguidores
	const float CRAWLER_SPEED = 20.0f; // lentos, rastreadores

	// How tough is each zombie type
	const float BLOATER_HEALTH = 5.0f; // hinchado, los más duros
	const float CHASER_HEALTH = 1.0f;	// cazador, los más debiles
	const float CRAWLER_HEALTH = 3.0f; // rastreador segundos mas duros

	// Make each zombie vary its speed slightly
	const int MAX_VARRIANCE = 30U;
	const int OFFSET = 101U - MAX_VARRIANCE;

	// Where is this zombie?
	sf::Vector2f m_Position;

	// A sprite for the zombie
	sf::Sprite m_Sprite;

	// How fast can this one run/crawl? corre/se arrastra
	float m_Speed;

	// How much health has it got?
	float m_Health;

	// Is it still alive?
	bool m_Alive;

// Public prototypes go here
public:
	// Handle when a bullet hits a zombie
	bool hit();

	// Find out if the zombie is alive
	bool isAlive();

	// Spawn a new zombie
	void spawn(float startX, float startY, int type, int seed);

	// Return a rectangle that is the position in the world
	sf::FloatRect getPosition();

	// Get a copy of the sprite to draw
	sf::Sprite getSprite();

	// Update the zombie each frame
	void update(float elapsedTime, sf::Vector2f playerLocation);
};
#endif /* INCLUDE_ZOMBIE_H_ */
